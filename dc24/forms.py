from django import forms


class VisaRequestPassportDetailsForm(forms.Form):
    form_title = {"name":"passport_section", "label":"Passport Details"}
    surname = forms.CharField(label="Surname", max_length=100, help_text="As stated in your passport")
    givennames = forms.CharField(label="Given names", max_length=100, help_text="As stated in your passport")
    passport_no = forms.CharField(label="Passport number", max_length=100)
    sex = forms.ChoiceField(label="Sex", choices=[("M","Male"),("F","Female")], widget=forms.RadioSelect, help_text="As stated in your passport")
    date_of_birth = forms.DateField(label="Date of Birth", widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    date_of_expiry = forms.DateField(label="Date of Expiry", widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    nationality = forms.CharField(label="Nationality", max_length=100, help_text="As stated in your passport")
    place_of_issue = forms.CharField(label="Place of issue", max_length=100)

class VisaRequestInviteeDetailsForm(forms.Form):
    form_title = {"name":"invitee_section", "label":"Invitee Details"}
    residency = forms.CharField(label="In which city do you currently live?", max_length=100)
    mission = forms.CharField(label="Where do you plan to apply for visa?", max_length=100, help_text="This should be either a South Korean diplomatic mission or Visa application center near you")
    is_abroad = forms.ChoiceField(label="Are you living your in home country or abroad?", choices=[
        ("HOME_COUNTRY","Home country"),
        ("ABROAD_LESS_1YEAR","Abroad, Less than a year with valid permit"),
        ("ABROAD_1YEARS_TO_2YEARS","Abroad, Between 1 and 2 years with valid permit"),
        ("ABAORD_MORE_THEN_2YEARS","Abroad, More than 2 years with valid permit"),
        ("STATELESS_OR_REFUGEE", "I'm stateless or refugee (Travel document or Travel certificate holders)")
        ], widget=forms.RadioSelect)
    occupation = forms.CharField(label="Occupation", max_length=100)
    affiliation = forms.CharField(label="Affiliation", max_length=100, help_text="e.g. employer")

class VisaRequestTravelDetailsForm(forms.Form):
    form_title = {"name":"travel_section", "label":"Travel Details"}
    date_of_arrival = forms.DateField(label="Date of Arrival", widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    date_of_departure = forms.DateField(label="Date of Departure", widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    accommodation = forms.ChoiceField(label="Accommodation", choices=[
        ("DEBCONF", "DebConf Accommodation"),
        ("OTHER", "Other (Self-arranged)")
        ], widget=forms.RadioSelect)
    address_in_korea = forms.CharField(max_length=100, widget=forms.Textarea, required=False,
        label="Address for self-arranged Accommodation", help_text="Please enter if you will be staying at self-arranged accommodation or staying longer before or after DebConf")
    funded_by = forms.ChoiceField(label="Who funds your travel?", choices=[
        ("BURSARY", "DebConf Bursary"),
        ("SELF", "Self funded"),
        ("FAMILY", "Family or relatives(parents, grandparents, etc)"),
        ("EMPLOYER", "Employer"),
        ("EDU", "School, College or other Educational institution"),
        ("SCHOLARSHIP", "Scholarship institution"),
        ("OTHER", "Other")
    ], widget=forms.RadioSelect)
    funding_items = forms.ChoiceField(label="Funding Items", choices=[
        ("ACCOMMODATION", "Accommodation"),
        ("FOOD", "Food"),
        ("TRAVEL", "Transportation"),
    ], widget=forms.CheckboxSelectMultiple)
    travel_costs_usd = forms.IntegerField(label="Estimated total travel costs", help_text="In USD")
    bursary_backup = forms.ChoiceField(label="Backup plan if bursary not fully accepted", choices=[
        ("SELF", "I'll self fund my travel"),
        ("HELP", "I'll be still able to fund travel with help from others"),
        ("SHORTEN", "I'll make travel period shorter"),
        ("CANCEL", "I'll cancel my travel")
    ], widget=forms.RadioSelect, required=False)
    when_to_start = forms.ChoiceField(label="When to start visa invitation process", choices=[
        ("SOON_TIGHT_SCHEDULE", "Soon, Due to my tight schedule before DebCamp or DebConf"),
        ("SOON_APPLY_TAKE_LONG_TIME", "Soon, As Visa application takes a long time to submit and be processed in my area"),
        ("AFTER_BURSARY", "After receiving bursary results, As bursaries are critical for my travel decisions"),
    ], widget=forms.RadioSelect, required=False)
    debconf_purpose = forms.ChoiceField(label="Your purpose or plan for DebCamp or DebConf", choices=[
        ("DEBCONF_TEAM", "Joining as part of DebConf team (Describe your role in the details question)"),
        ("SPEAK", "Plan to present a talk (Enter topic in details question)"),
        ("DISCUSS", "To discuss or work on specific agenda on Debian project (Enter agenda to discuss on details question)"),
        ("SPONSOR", "To engage with the community as a sponsor"),
        ("VOLUNTEER", "To volunteer at DebCamp or DebConf (Enter what you will be volunteering on details question)"),
        ("SESSION", "To participate session with specific topic (Enter session topic you interested on details question)"),
        ("KEYSIGNING", "To join keysigning party"),
    ], widget=forms.CheckboxSelectMultiple)
    debconf_purpose_details = forms.CharField(label="Details of your purpose or plan for DebCamp or DebConf", max_length=100, required=False)

class VisaRequestGuaranteeDetailsForm(forms.Form):
    form_title = {"name":"guarantee_section", "label":"Visa Guarantee Details"}
    garantee_required = forms.ChoiceField(label="Do you require visa guarantee?", choices=[
        ("NO", "No"),
        ('WEBSITE', "Yes, It was stated in the website."),
        ("INFORMED", "Yes, It was not stated in the website, But they informed me to submit it for some reason."),
        ("COMBINED", "No, But they require specific text that guarantees that invitee will return to home country in the invitation letter."),
        ("BACKUP", "No, But I would like to request one due to my unsteady income. (or no steady income yet)")
    ], widget=forms.RadioSelect)
    phone_number = forms.CharField(label="Enter your phone number if you need letter of guarantee from us.", max_length=100, required=False)
    insurance = forms.ChoiceField(label="Have you signed up for travel insurance?", widget=forms.RadioSelect, required=False,
                                  help_text="For people need letter of guarantee, Visa sponsor require to sign up for travel insurance to minimize potential legal risks from providing guarantee.\
                                    Note that your travel insurance need to cover personal liability.",
                                  choices=[
                                      ("YES", "Yes"),
                                      ("NO", "Not yet, Will sign up soon.")])