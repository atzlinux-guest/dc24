---
name: 2024 데비안 컨퍼런스(DebConf24)
---

# 2024 데비안 컨퍼런스(DebConf24)

2024 데비안 컨퍼런스(DebConf24)는 데비안 프로젝트의 연례 개발자 컨퍼런스 입니다.

장소 : 국립부경대학교 대연캠퍼스

| 행사 | 기간 | 설명 |
|:----:|:----:|:----:|
| [DebCamp](/about/debcamp) | 2024.07.21(일) ~ 07.27 (토) | 데비안 OS, 관련 프로젝트 개발 기간 |
| [DebConf](/ko/debconf) | 2024.07.28(일) ~ 08.04 (일) | 데비안 컨퍼런스 |

<img class="img-fluid" src="{% static "img/dc24-ko-poster.jpg" %}"
     title="DebConf24 Korean Poster">
