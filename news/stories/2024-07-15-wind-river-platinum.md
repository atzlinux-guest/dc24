---
title: Wind River Platinum Sponsor of DebConf24
---

We are pleased to announce that [Wind River](https://www.windriver.com) has committed to sponsor [DebConf24](https://debconf24.debconf.org/) as a **Platinum Sponsor**.

For nearly 20 years, Wind River has led in commercial open source Linux solutions for mission-critical enterprise edge computing. With expertise across aerospace, automotive, industrial, telecom, more, the company is committed to open source through initiatives like eLxr, Yocto, Zephyr, and StarlingX.

With this commitment as Platinum Sponsor, Wind River is contributing to make possible our annual conference, and directly supporting the progress of Debian and Free Software, helping to strengthen the community that continues to collaborate on Debian projects throughout the rest of the year.

Wind River plans to announce an exiting new project based on Debian at this year's DebConf!

Thank you very much, Wind River, for your support of DebConf24!
