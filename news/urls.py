from django.urls import re_path

from news.views import NewsAtomView, NewsItemView, NewsFeedView, NewsRSSView


urlpatterns = [
    re_path(r'^(?:page/(?P<page>[0-9]+)/)?$',
        NewsFeedView.as_view(), name='news'),
    re_path(r'^(?P<date>[0-9]{4}-[0-9]{2}-[0-9]{2})-(?P<slug>[^/]+)/$',
        NewsItemView.as_view(), name='news_item'),
    re_path(r'^feed/rss.xml$',
        NewsRSSView(), name='news_rss'),
    re_path(r'^feed/atom.xml$',
        NewsAtomView(), name='news_atom'),
]
